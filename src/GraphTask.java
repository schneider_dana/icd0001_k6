import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.lang.System.getProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Container class to different classes, that makes the whole set of classes one class formally.
 */
public class GraphTask {

  /**
   * Main method.
   */
  public static void main(String[] args) {
    GraphTask a = new GraphTask();
    a.run();
  }

  /**
   * Actual main method to run examples and everything.
   */
  public void run() {
    /*
     Create a graph manually for part of the test cases
     Created graph:
     - is oriented
     - each vertex is connected to at least one other vertex
     - has no loops
     */

    Graph graph1 = new Graph("Graph1");

    Vertex vertexA = new Vertex("A");
    Vertex vertexB = new Vertex("B");
    Vertex vertexC = new Vertex("C");
    Vertex vertexD = new Vertex("D");

    graph1.setFirst(vertexA);
    vertexA.setNext(vertexB);
    vertexB.setNext(vertexC);
    vertexC.setNext(vertexD);

    Arc AtoB = new Arc("A->B");
    AtoB.setTarget(vertexB);
    Arc AtoC = new Arc("A->C");
    AtoC.setTarget(vertexC);
    Arc BtoC = new Arc("B->C");
    BtoC.setTarget(vertexC);
    Arc CtoD = new Arc("C->D");
    CtoD.setTarget(vertexD);

    vertexA.setFirst(AtoB);
    AtoB.setNext(AtoC);
    vertexB.setFirst(BtoC);
    vertexC.setFirst(CtoD);


    /*
    First test case: using manually created graph

    Find path from A to D.
    Both points exist in graph and should have 2 paths available
     (given in the order they are expected to be returned!):
    A->C->D
    A->B->C->D

    Found paths list size should be equal to 2.
     */
    System.out.println(getProperty("line.separator") + "First test case" + getProperty("line.separator"));
    System.out.println("Created graph is:" + graph1.toString());
    System.out.println("Total number of possible paths from A to D: " + graph1.getAllPaths("A", "D").size());


    /*
    Second test case: using manually created graph

    Find path from D to A.
    Both points exist in graph, but have no path available,
     because no arcs are directed *out* of vertex D

    Should print out that no paths are found.
    Found paths list size should be equal to 0.
     */
    System.out.println(getProperty("line.separator") + "Second test case" + getProperty("line.separator"));
    System.out.println("Created graph is:" + graph1.toString());
    System.out.println("Total number of possible paths from D to A: " + graph1.getAllPaths("D", "A").size());


    /*
    Third test case: using manually created graph

    Find path from:
    - G to C - start point does not exist
    - B to F - end point does not exist
    - random1 to random2 - both points do not exist

    Should throw an exception on each call with message that given vertices do not exist.
    For code to continue running, exceptions are caught and printed out as usual messages.
    */
    System.out.println(getProperty("line.separator") + "Third test case" + getProperty("line.separator"));
    System.out.println("Created graph is:" + graph1.toString());

    try {
      graph1.getAllPaths("G", "C");
    } catch (IllegalArgumentException e) {
      System.out.println("Exception thrown: " + e);
    }

    try {
      graph1.getAllPaths("B", "F");
    } catch (IllegalArgumentException e) {
      System.out.println("Exception thrown: " + e);
    }

    try {
      graph1.getAllPaths("random1", "random2");
    } catch (IllegalArgumentException e) {
      System.out.println("Exception thrown: " + e);
    }


    /*
    Fourth test case: using manually created graph

    Find path from B to B.

    Should print out that given points are same point.
    Found paths list size should be equal to 0.
    */
    System.out.println(getProperty("line.separator") + "Fourth test case" + getProperty("line.separator"));
    System.out.println("Created graph is:" + graph1.toString());
    System.out.println("Total number of possible paths from B to B: " + graph1.getAllPaths("B", "B").size());


    /*
    Fifth test case: using createRandomSimpleGraph.

    Output will be depending on randomly created graph, but can be checked by looking at printed out graph.

    As created graph is not oriented and each vertex has connection to at least one other vertex,
     then at least one path should be found (when vertices are existing in the graph)
    */
    System.out.println(getProperty("line.separator") + "Fifth test case" + getProperty("line.separator"));
    Graph graph2 = new Graph("Graph2");
    graph2.createRandomSimpleGraph(5, 9);

    System.out.println("Created graph is:" + graph2.toString());

    // should find at least one path
    System.out.println("Total number of possible paths from v1 to v4: "
        + graph2.getAllPaths("v1", "v4").size());

    // should throw an exception since v8 cannot exist in graph with 5 vertices
    try {
      graph2.getAllPaths("v1", "v8");
    } catch (IllegalArgumentException e) {
      System.out.println("Exception caught: " + e);
    }


    /*
    Sixth test case: using createRandomGraph.

    Test case will provide createRandomGraph method with 0 for number of arcs to create.

    As no vertices will be connected, should print out that no paths are found.
    Found paths list size should be equal to 0.
    */
    System.out.println(getProperty("line.separator") + "Sixth test case" + getProperty("line.separator"));
    Graph graph3 = new Graph("Graph3");
    graph3.createRandomGraph(10, 0);

    System.out.println("Created graph is:\n" + graph3.toString());

    // both points exist, but not paths available
    System.out.println("Total number of possible paths from v3 to v8: "
        + graph3.getAllPaths("v3", "v8").size());


    /*
    Seventh test case: using createRandomGraph.

    Output will be depending on randomly created graph, but can be checked by looking at printed out graph.

    Since method allows multigraphs and loops,
     there always is a chance that there are no paths between vertices given to getAllPaths method
    */
    System.out.println(getProperty("line.separator") + "Seventh test case" + getProperty("line.separator"));
    Graph graph4 = new Graph("Graph4");
    graph4.createRandomGraph(7, 15);

    System.out.println("Created graph is:\n" + graph4.toString());

    // should either find at least one path or print out that no paths possible
    System.out.println("Total number of possible paths from v5 to v2: "
        + graph4.getAllPaths("v5", "v2").size());

    // should throw an exception since v15 cannot exist in graph with 7 vertices
    try {
      graph4.getAllPaths("v1", "v15");
    } catch (IllegalArgumentException e) {
      System.out.println("Exception caught: " + e);
    }


    /*
    Eighth test case: using createRandomSimpleGraph.

    Created graph will have over 2000 vertices.

    To make sure that on each test case run, there is at least one path available
     (which ensures that built solution works), preferred to use *simple* graph for this test case.

    Output will be depending on randomly created graph, but can be checked by looking at printed out graph.

    As created graph is not oriented and each vertex has connection to at least one other vertex,
     then at least one path should be found (when vertices are existing in the graph)
    */
    System.out.println(getProperty("line.separator") + "Eighth test case" + getProperty("line.separator"));
    Graph graph5 = new Graph("Graph5");
    graph5.createRandomSimpleGraph(2150, 2150);

    System.out.println("Created graph is:\n" + graph5.toString());

    // should find at least 1 path; prints out time
    long t0 = currentTimeMillis();
    System.out.println("Total number of possible paths from v10 to v2110: "
        + graph5.getAllPaths("v10", "v2110").size());
    long t1 = currentTimeMillis();
    System.out.println("Total time spent on going through the graph: " + (int)(t1 - t0) + "ms");

  }

  /**
   * Vertex represents one point in the graph.
   */
  class Vertex {

    private String id;
    private Vertex next;
    private Arc first;
    private int info = 0;
    /**
     * A flag that helps to keep a track of whether the current point has been visited while following a
     * path.
     */
    private boolean visited = false;

    Vertex(String s, Vertex v, Arc e) {
      id = s;
      next = v;
      first = e;
    }

    Vertex(String s) {
      this(s, null, null);
    }

    @Override
    public String toString() {
      return id;
    }

    /**
     * <p>
     *   Method that returns all vertices that are direct targets of the current vertex
     * </p>
     *
     * @return list of vertices that are directly pointed at from current vertex
     */
    public List<Vertex> getAllTargets() {
      List<Vertex> targets = new ArrayList<>();
      Arc currentArc = first;

      while (currentArc != null) {
        if (currentArc.getTarget() != null) {
          targets.add(currentArc.getTarget());
          currentArc = currentArc.getNext();
        }
      }

      return targets;
    }

    /**
     * <p>
     *   Clones current vertex by creating new instance with same parameters
     * </p>
     *
     * @return Vertex with same parameters as current one
     */
    @Override
    public Vertex clone() {
      return new Vertex(id, next, first);
    }

    /**
     * <p>
     *   A special getter method for {@code boolean} field.
     * </p>
     *
     * @return value of field visited
     */
    public boolean isVisited() {
      return visited;
    }

    public void setVisited(boolean value) {
      visited = value;
    }

    public String getId() {
      return id;
    }

    public Vertex getNext() {
      return next;
    }

    public void setNext(Vertex next) {
      this.next = next;
    }

    public Arc getFirst() {
      return first;
    }

    public void setFirst(Arc first) {
      this.first = first;
    }

    public int getInfo() {
      return info;
    }

    public void setInfo(int info) {
      this.info = info;
    }
  }

  /**
   * Arc represents one arrow in the graph. Two-directional edges are represented by two Arc objects (for both
   * directions).
   */
  class Arc {

    private String id;
    private Vertex target;
    private Arc next;
    private int info = 0;

    Arc(String s, Vertex v, Arc a) {
      id = s;
      target = v;
      next = a;
    }

    Arc(String s) {
      this(s, null, null);
    }

    @Override
    public String toString() {
      return id;
    }

    public String getId() {
      return id;
    }

    public Vertex getTarget() {
      return target;
    }

    public void setTarget(Vertex target) {
      this.target = target;
    }

    public Arc getNext() {
      return next;
    }

    public void setNext(Arc next) {
      this.next = next;
    }

    public int getInfo() {
      return info;
    }

    public void setInfo(int info) {
      this.info = info;
    }

  }

  class Graph {

    private String id;
    private Vertex first;
    private int info = 0;
    /**
     * A private list to keep track of found possible paths when traversing from one point to another
     */
    private List<List<Vertex>> possiblePaths;

    Graph(String s, Vertex v) {
      id = s;
      first = v;
    }

    Graph(String s) {
      this(s, null);
    }

    public String getId() {
      return id;
    }

    public Vertex getFirst() {
      return first;
    }

    public void setFirst(Vertex first) {
      this.first = first;
    }

    public int getInfo() {
      return info;
    }

    public void setInfo(int info) {
      this.info = info;
    }

    /**
     * <p>
     *   An overriding method.
     *   Builds textual representation for the graph.
     * </p>
     *
     * @return String representation of the graph
     */
    @Override
    public String toString() {
      String nl = getProperty("line.separator");
      StringBuilder sb = new StringBuilder(nl);
      sb.append(getId());
      sb.append(nl);
      Vertex v = getFirst();
      while (v != null) {
        sb.append(v.toString());
        sb.append(" -->");
        Arc a = v.getFirst();
        while (a != null) {
          sb.append(" ");
          sb.append(a.toString());
          sb.append(" (");
          sb.append(v.toString());
          sb.append("->");
          sb.append(a.getTarget().toString());
          sb.append(")");
          a = a.getNext();
        }
        sb.append(nl);
        v = v.getNext();
      }
      return sb.toString();
    }

    /**
     * <p>
     *   Creates new Vertex with given {@code id}.
     *   Sets its next vertex to be current graph's first vertex.
     *   Sets current graph's first Vertex to be newly created vertex.
     *   Returns newly created vertex.
     * </p>
     *
     * @param vid id for new Vertex
     * @return created Vertex
     */
    private Vertex createVertex(String vid) {
      Vertex res = new Vertex(vid);
      res.setNext(getFirst());
      setFirst(res);
      return res;
    }

    /**
     * <p>
     *   Creates new Arc from given start Vertex {@code from} to given end Vertex {@code to}.
     *   Sets newly created arc's next Arc to be start vertex's first Arc and target Vertex to be end vertex.
     *   Sets start vertex's first Arc to be newly created arc.
     *   Returns newly created arc.
     *   </p>
     *
     * @param aid id for new Arc
     * @param from arc start Vertex
     * @param to arc end Vertex
     * @return created Arc
     */
    private Arc createArc(String aid, Vertex from, Vertex to) {
      Arc res = new Arc(aid);
      res.setNext(from.getFirst());
      from.setFirst(res);
      res.setTarget(to);
      return res;
    }

    /**
     * Create a connected undirected random tree with n vertices.
     * Each new vertex is connected to some random existing vertex.
     *
     * @param n number of vertices added to this graph
     */
    public void createRandomTree(int n) {
      if (n <= 0) {
        return;
      }
      Vertex[] varray = new Vertex[n];
      for (int i = 0; i < n; i++) {
        varray[i] = createVertex("v" + (n - i));
        if (i > 0) {
          int vnr = (int) (Math.random() * i);
          createArc("a" + varray[vnr].toString() + "_"
              + varray[i].toString(), varray[vnr], varray[i]);
          createArc("a" + varray[i].toString() + "_"
              + varray[vnr].toString(), varray[i], varray[vnr]);
        }
      }
    }

    /**
     * Create an adjacency matrix of this graph.
     * Side effect: corrupts info fields in the graph
     *
     * @return adjacency matrix
     */
    public int[][] createAdjMatrix() {
      setInfo(0);
      Vertex v = getFirst();
      while (v != null) {
        v.setInfo(info++);
        v = v.getNext();
      }
      int[][] res = new int[info][info];
      v = getFirst();
      while (v != null) {
        int i = v.getInfo();
        Arc a = v.getFirst();
        while (a != null) {
          int j = a.getTarget().getInfo();
          res[i][j]++;
          a = a.getNext();
        }
        v = v.getNext();
      }
      return res;
    }

    /**
     * Create a connected simple (undirected, no loops, no multiple arcs) random graph with n vertices
     * and m edges.
     *
     * @param n number of vertices
     * @param m number of edges
     * @throws IllegalArgumentException
     *         If {@code n} is smaller or equal to 0 or bigger than 2500.
     *         If {@code m} is smaller than n - 1 or bigger than n * (n - 1) / 2.
     */
    public void createRandomSimpleGraph(int n, int m) {
      if (n <= 0) {
        return;
      }
      if (n > 2500) {
        throw new IllegalArgumentException("Too many vertices: " + n);
      }
      if (m < n - 1 || m > n * (n - 1) / 2) {
        throw new IllegalArgumentException
            ("Impossible number of edges: " + m);
      }
      setFirst(null);
      createRandomTree(n);       // n-1 edges created here
      Vertex[] vert = new Vertex[n];
      Vertex v = getFirst();
      int c = 0;
      while (v != null) {
        vert[c++] = v;
        v = v.getNext();
      }
      int[][] connected = createAdjMatrix();
      int edgeCount = m - n + 1;  // remaining edges
      while (edgeCount > 0) {
        int i = (int) (Math.random() * n);  // random source
        int j = (int) (Math.random() * n);  // random target
        if (i == j) {
          continue;  // no loops
        }
        if (connected[i][j] != 0 || connected[j][i] != 0) {
          continue;  // no multiple edges
        }
        Vertex vi = vert[i];
        Vertex vj = vert[j];
        createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
        connected[i][j] = 1;
        createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
        connected[j][i] = 1;
        edgeCount--;  // a new edge happily created
      }
    }

    /**
     * <p>
     *   Uses {@code createRandomSimpleGraph} method as base.
     *   Creates a random graph with {@code numberOfVertices} vertices and {@code numberOfArcs} arcs.
     *   As opposed to {@code createRandomSimpleGraph} method,
     *   allows to create not fully connected, directed graph with loops and multiple arcs.
     * </p>
     *
     * @param numberOfVertices number of vertices to create
     * @param numberOfArcs number of arcs to create
     * @throws IllegalArgumentException
     *         If {@code numberOfVertices} is smaller or equal to 0 or bigger than 2500.
     *         If {@code numberOfArcs} is smaller than 0.
     */
    public void createRandomGraph(int numberOfVertices, int numberOfArcs) {
      if (numberOfVertices <= 0 || numberOfArcs < 0) {
        throw new IllegalArgumentException(
            format("Cannot build graph with %d number of vertices and %d number of arcs, "
                    + "because neither of the numbers can be negative and number of vertices cannot be 0.",
                numberOfVertices, numberOfArcs)
        );
      }
      if (numberOfVertices > 2500) {
        throw new IllegalArgumentException(
            format("Too many vertices: %d."
                    + "Please provide numbers smaller than 2500.",
                numberOfVertices, numberOfArcs)
        );
      }

      Vertex[] vertices = new Vertex[numberOfVertices];

      for (int i = numberOfVertices - 1; i >= 0; i--) {
        Vertex vertex = createVertex("v" + (i + 1));
        vertices[i] = vertex;
      }

      for (int i = 0; i < numberOfArcs; i++) {
        int sourceIndex = (int) (Math.random() * numberOfVertices);  // random source
        int targetIndex = (int) (Math.random() * numberOfVertices);  // random target

        Vertex source = vertices[sourceIndex];
        Vertex target = vertices[targetIndex];

        createArc(format("a_%s-%s", source, target), source, target);
      }
    }

    /**
     * <p>
     *   Finds and returns all possible paths from Vertex with id {@code fromId}
     *   to Vertex with id {@code toId}.
     *   Method is case-sensitive, so given ids should match existing ids by case.
     *   If both given ids are equal, returns empty list with message that given ids
     *   are referencing same point.
     *   If no paths were found, returns empty list with message that there are no possible paths
     *   between give from and to points.
     *   If found more than one path, prints each path out before returning the list of paths.
     * </p>
     *
     * @param fromId {@code String} id of a start Vertex
     * @param toId {@code String} id of an end Vertex
     * @return list of all found possible paths or empty list if none found
     * @throws IllegalArgumentException
     *         If either or both given ids are non-existent among all vertices' ids in current graph.
     */
    public List<List<Vertex>> getAllPaths(String fromId, String toId) {
      List<String> graphVerticesIds = getAllVerticesIds();

      if (!graphVerticesIds.contains(fromId) || !graphVerticesIds.contains(toId)) {
        throw new IllegalArgumentException(
            format("Cannot find path from \"%s\" to \"%s\", "
                + "because either one or both of the points do not exist in this graph.", fromId, toId)
        );
      }

      if (fromId.equals(toId)) {
        System.out.println(format("Given points from \"%s\" to \"%s\" are the same point.", fromId, toId));
        return new ArrayList<>();
      }

      possiblePaths = new ArrayList<>();

      Vertex startVertex = findVertexById(fromId);
      Vertex endVertex = findVertexById(toId);

      List<Vertex> cachePath = new ArrayList<>();
      cachePath.add(startVertex);

      depthFirstSearch(startVertex, endVertex, cachePath);

      if (possiblePaths.size() == 0) {
        System.out.println(format("There are no possible paths from \"%s\" to \"%s\"", fromId, toId));
      } else {
        System.out
            .println(format("To get from \"%s\" to \"%s\", following paths can be followed:", fromId, toId));
        possiblePaths.forEach(path -> {
          StringBuilder sb = new StringBuilder();

          path.forEach(stop -> {
            if (path.indexOf(stop) == path.size() - 1) {
              sb.append(stop);
              return;
            }

            sb.append(stop);
            sb.append(" -> ");
          });

          System.out.println(sb.toString());
        });
      }

      return copyAllPaths(possiblePaths);
    }

    /**
     * <p>
     *   Recursive method that passes graph depth first from vertex {@code currentVertex}
     *   to vertex {@code targetVertex} finding each possible path.
     *   Once one path found, saves a copy of it to Graph's possiblePaths list.
     *
     *   Returns either when found {@code targetVertex},
     *   when all targets of {@code currentVertex} have been passed or
     *   when {@code currentVertex} doesn't have any targets.
     *
     *   Base logic of the method taken from
     *   <a href="https://www.geeksforgeeks.org/find-paths-given-source-destination/">
     *     GeeksforGeeks article
     *   </a>
     * </p>
     *
     * @param currentVertex current stop on the path
     * @param targetVertex final destination
     * @param path constantly modified parameter to memorise passed path
     */
    private void depthFirstSearch(Vertex currentVertex, Vertex targetVertex, List<Vertex> path) {
      currentVertex.setVisited(true);

      if (currentVertex.equals(targetVertex)) {
        List<Vertex> pathCopy = copyPathFrom(path);
        possiblePaths.add(pathCopy);

        currentVertex.setVisited(false);
        return;
      }

      List<Vertex> targets = currentVertex.getAllTargets();

      for (int i = targets.size() - 1; i > -1; i--) {
        Vertex vertex = targets.get(i);

        if (!vertex.isVisited()) {
          path.add(vertex);
          depthFirstSearch(vertex, targetVertex, path);
          path.remove(vertex);
        }
      }

      currentVertex.setVisited(false);
    }

    /**
     * <p>Copies given list of vertices by creating a new list with clones of each Vertex in given list.</p>
     *
     * @param list list to copy
     * @return copy of given list
     */
    private List<Vertex> copyPathFrom(List<Vertex> list) {
      List<Vertex> copy = new ArrayList<>();

      list.forEach(
          vertex -> copy.add(vertex.clone())
      );

      return copy;
    }

    /**
     * <p>
     *   Copies given list of paths (each path is a list of vertices) by creating a new list
     *   with copy of each inside list made through {@code copyPathFrom} method.
     * </p>
     *
     * @param list list of paths to copy (where each path is a list of vertices)
     * @return copy of given list
     */
    private List<List<Vertex>> copyAllPaths(List<List<Vertex>> list) {
      List<List<Vertex>> copy = new ArrayList<>();

      list.forEach(path -> {
        List<Vertex> pathCopy = copyPathFrom(path);
        copy.add(pathCopy);
      });

      return copy;
    }

    /**
     * <p>Finds all ids of vertices in current graph.</p>
     *
     * @return list of ids of vertices in current graph
     */
    private List<String> getAllVerticesIds() {
      List<String> vertices = new ArrayList<>();
      Vertex currentVertex = getFirst();
      while (currentVertex != null) {
        vertices.add(currentVertex.getId());
        currentVertex = currentVertex.getNext();
      }
      return vertices;
    }
    /**
     * <p>Finds all vertices in current graph.</p>
     *
     * @return list of vertices in current graph
     */
    private List<Vertex> getAllVertices() {
      List<Vertex> vertices = new ArrayList<>();
      Vertex currentVertex = getFirst();
      while (currentVertex != null) {
        vertices.add(currentVertex);
        currentVertex = currentVertex.getNext();
      }
      return vertices;
    }

    /**
     * <p>
     *   Finds an exact Vertex by given {@code id}.
     *   Does not consider edge case when graph has several vertices with same ids -
     *   will return last found vertex.
     * </p>
     *
     * @param id id of a Vertex to find
     * @return last or only {@code Vertex} found in current graph with given {@code id}
     */
    private Vertex findVertexById(String id) {
      final Vertex[] response = new Vertex[1];

      getAllVertices().forEach(vertex -> {
        if (vertex.getId().equals(id)) {
          response[0] = vertex;
        }
      });

      return response[0];
    }
  }
}
